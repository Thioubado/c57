<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Acme\Crm\Models;

use Model;

/**
 * Opportunity Model.
 */
class Opportunity extends Model
{
    /**
     * @var string the database table used by the model
     */
    public $table = 'acme_crm_opportunities';

    /**
     * @var array Relations
     */
    public $hasMany = [
        'notes' => ['Acme\Crm\Models\Note'],
    ];

    public $belongsTo = [
        'status' => ['Acme\Crm\Models\Status'],
    ];

    public $belongsToMany = [
        'contacts' => [
            'Acme\Crm\Models\Contact',
            'table' => 'acme_crm_opportunities_contacts',
        ],
        'offerings' => [
            'Acme\Crm\Models\Offering',
            'table'      => 'acme_crm_opportunities_offerings',
            'pivot'      => ['price', 'cost', 'owner_id'],
            'pivotModel' => 'Acme\Crm\Models\OpportunityOfferingPivot',
        ],
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];
}
