<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Php\Components;

/**
 * Donne les tables de multiplication de 1 à 12 max.
 *
 * Exemple: 2 façons de commander la table de 7:
 * $table = new TableDe(7)
 * ou
 * $table->de7;
 *
 * @var n int [1-12]
 */
class TableDe
{
    public function __construct($n = null)
    {
        $this->result = ($n && $this->is_valid($n)) ? $this->getTable($n) : 'Bad commande!';
    }

    public function __get($table)
    {
        $ope            = substr($table, 0, 2);
        $multiplicateur = substr($table, 2);

        if ('de' === $ope && $this->is_valid($multiplicateur)) {
            $nom    = 'de'.$multiplicateur;
            ${$nom} = $this->creerFonctionMultiplier($multiplicateur);
            $foiss  = range(0, 12);
            $lignes = ['Table de '.$multiplicateur];

            foreach ($foiss as $fois) {
                $lignes[] = ${$nom}($fois);
            }

            return $lignes;
        }

        return 'Bad commande!';
    }

    public function creerFonctionMultiplier($n)
    {
        $multiplier = function ($i) use ($n) {
            return $i.' X '.$n.' = '.$i * $n;
        };

        return $multiplier;
    }

    public function getTable($n)
    {
        for ($i = 0; $i < 13; ++$i) {
            $lignes[] = $i.' X '.$n.'  = '.($i * $n);
        }

        return $lignes;
    }

    public function init()
    {
        // $result   = ['Users:'];
        // $result[] = $this->users();

        return $this->exempleClosureDansFonction();
    }

    public function exempleClosureDansFonction()
    {
        $ajouterCinq = $this->creerFonctionAjouter(5);
        $ajouterSept = $this->creerFonctionAjouter(7);

        return ['Closure dans fonction:', $ajouterCinq(1514) + $ajouterSept(777)];
    }

    public function creerFonctionAjouter($quantite)
    {
        $ajouter = function ($nombre) use ($quantite) {
            return $nombre + $quantite;
        };

        return $ajouter;
    }

    protected function is_valid($n)
    {
        if ($n > 0 && $n < 13) {
            return true;
        }
    }
}
