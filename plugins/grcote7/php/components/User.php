<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\Php\Components;

use Exception;
use GrCote7\Demos\Divers\Todoc57\Models\Task;
use Illuminate\Support\Facades\DB;
use RainLab\User\Models\User as UserRL;

class User extends UserRL
{
    public function __construct()
    {
        if (!(\defined('SEPA'))) {
            \define('SEPA', str_repeat('- ', 15));
        }
    }

    public static function oneUser($id = null)
    {
        $titles = Db::table('grcote7_tododb_tasks')->lists('title', 'id');

        foreach ($titles as $id => $title) {
            $r[] = $title.' (id '.$id.')';
        }

        $user = self::first();
        $r[]  = '1er user: '.$user->name;

        $id = $id ?? 2;

        try {
            $user = self::findOrFail($id); // Lance une exception
            $r[]  = '1 user (id='.$id.'): '.$user->name;
        } catch (Exception $e) {
            $r[] = 'ATTENTION: Pas d\'user avec l\'id '.$id;
        }

        $pseudo_users     = self::where('surname', '<>', '');
        $pseudo_users_ori = clone $pseudo_users;
        $count            = $pseudo_users->count();
        $selection        = $pseudo_users->first();

        $r[] = SEPA;
        $r[] = ($selection) ? '1er user qui a un surnom: '.$selection->name.' ('.$selection->surname.')' : 'Aucun user n\'a de surnom';

        $r[] = ($count > 1) ? $count.' users ont des surnoms' : ((1 === $count) ? '1 seul user a un surnom' : 'Aucun user n\'a de surnom');

        $users = $pseudo_users_ori->lists('surname', 'name');

        foreach ($users as $name => $surname) {
            $r[] = $name.' ('.ucfirst($surname).')';
        }

        // TodosDB

        $tasks = DB::table('grcote7_tododb_tasks')->where('id', '>', 2);

        $todos = $tasks->select('id', 'title')->get();
        $r[]   = SEPA;
        $r[]   = 'Toutes les tasks';
        // var_dump($todos);
        // $r[] = $todos;
        foreach ($todos as $todo) {
            $r[] = $todo->id.' : '.$todo->title;
        }

        $tasks->delete();

        $tasks = Task::get();
        $r[]   = SEPA;
        $r[]   = 'Toutes les tasks';
        foreach ($tasks as $task) {
            $r[] = $task->id.' : '.$task->title;
        }

        return $r;
    }

    public function someUsers($n)
    {
        $users = UserRL::orderBy('name');
        $users = ($n) ? $users->take($n)->get() : $users->get();

        // var_dump($users);

        $r[] = 'someUsers():';
        foreach ($users as $user) {
            $r[] = $user->name;
        }
        $r[] = SEPA;

        $r[]   = 'Users selon tri';
        $users = Db::table('users')
            ->select(Db::raw('count(*) as user_count, bio'))
            ->where('bio', '<>', '')
            ->groupBy('bio')
            ->get();
        $r[] = $users[0];

        return $r;
    }

    public static function lotUsers()
    {
        $users = self::all();

        // Normalement directement en twig
        foreach ($users->chunk(3) as $chunk) {
            $r[] = '<div class="row">';
            foreach ($chunk as $user) {
                $r[] = '<div class="col-xs-4">'.$user->name.'</div>';
            }
            $r[] = '</div>';
        }

        return ['lotUsers (Par lot - chunk):', $r];
    }
}
