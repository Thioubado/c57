<?php namespace GrCOTE7\C57\Components;

use Cms\Classes\ComponentBase;

class Gc7StaticMenu extends ComponentBase
{

    public $items;

    public function componentDetails()
    {
        return [
            'name'        => 'MenuC57',
            'description' => 'Menu spécial pour c57.',
        ];
    }

    public function defineProperties()
    {
        return [
        ];
    }

    public function onRun()
    {

        // $this->items = [
        //     [ "name" => "Oki",
        //       "age" => 45
        //     ]
        // ];

        // $this->items = collect([1,2,3]);
        $this->items = collect(

            [
                [ "name" => "Pierre",
                  "age"  => 31,
                ]
            ]

        );

        // var_dump($this->items);

        // $this->addCss('assets/css/derInscrits.css');
        // $this->derinscrits = $this->loadDerInscrits();
        // var_dump($this->derinscrits);
    }

}
