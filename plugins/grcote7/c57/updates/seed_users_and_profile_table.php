<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace GrCOTE7\C57\Updates;

use Carbon\Carbon;
use GrCOTE7\Profile\Models\Profile;
use Illuminate\Support\Facades\DB;
use October\Rain\Database\Updates\Seeder;
use RainLab\User\Models\User;

// Normalement SeedUsersAndProfileTable
class seed_users_and_profile_table extends Seeder
{
    public function run()
    {
        DB::table('users')->truncate();

        $password = '$2y$10$6LMUI6DOwwXLXrx05cI39euY8ofkq8g3wQKd0rTp4E9MW3fXBnpsG';

        User::create([
            'name'                  => 'GC7',
            'email'                 => 'GrCOTE7@gmail.com',
            'password'              => $password,
            'password_confirmation' => $password,
            'is_activated'          => 1,
            'activated_at'          => Carbon::now(),
        ]);

        User::create([
            'name'                  => 'Jane',
            'email'                 => 'JaneDoe@gmail.com',
            'password'              => $password,
            'password_confirmation' => $password,
            'is_activated'          => 1,
            'activated_at'          => Carbon::now(),
        ]);

        User::create([
            'name'                  => 'Lio',
            'email'                 => 'Lio@gmail.com',
            'password'              => $password,
            'password_confirmation' => $password,
            'is_activated'          => 1,
            'activated_at'          => Carbon::now(),
        ]);

        // Tbale Profile
        DB::table('grcote7_profile_profiles')->truncate();

        Profile::create([
            'user_id' => 1,
            'pseudo'  => 'GC7',
            'parr'    => 'None',
            'sexe'    => 'H',
        ]);
        Profile::create([
            'user_id' => 2,
            'pseudo'  => 'Jane',
            'parr'    => 'GC7',
            'sexe'    => 'F',
        ]);
        Profile::create([
            'user_id' => 3,
            'pseudo'  => 'Lio',
            'parr'    => 'GC7',
            'sexe'    => 'H',
        ]);
    }
}
