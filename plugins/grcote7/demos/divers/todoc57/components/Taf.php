<?php namespace GrCOTE7\Demos\Divers\ToDoC57\Components;

use Cms\Classes\ComponentBase;
use GrCOTE7\Demos\Divers\ToDoC57\Models\Task;

class Taf extends ComponentBase
{
    /**
     * Person's name
     * @var string
     */
    public $name;

    /**
     * The collection of tasks
     * @var array
     */
    public $tasks;

    public function componentDetails()
    {
        return [
            'name'        => 'TAF Component',
            'description' => 'A database driven ToDo list...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){

        // {{ todoList.name }} (Strict)
        $this->name         = 'Lionel';
        // {{ name }} (Relaxed)
        $this->page['name'] = 'Lio181';

        $this->tasks        = Task::lists('title');

        // var_dump($this->tasks);
    }
    
    public function onAddItem(){
        
        $taskName       = post('task'); // request
        $task           = new Task;
        $task->title    = $taskName;
        $task->save();
        
        $this->page['tasks'] = Task::lists('title');
    }

    protected function getTasks(){
        return $this->page['tasks'] = Task::lists('title');
    }
}
