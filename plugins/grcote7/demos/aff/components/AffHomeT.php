<?php namespace GrCOTE7\Demos\Aff\Components;

use Cms\Classes\ComponentBase;

class AffHomeT extends ComponentBase
{
    public function componentDetails()
    {

        return [
            'name'        => 'Aff Home T',
            'description' => 'Implémente l\'affichage d\'un cicône Home en haut à droite (Fixe).',
        ];
    }

    public function onRun()
    {
        $this->addCss('components/affhomet/assets/css/style.css');
        // This code will note execute AJAX events.
        // {{ demoAff.name }} (strict) => Never conflict
        $this->name = 'Lionel';
        $this->role = 'Admin';
        // {% set name = name %}

        // {{ name }} (Relaxed)
        $this->page['homeT'] = 'HomeT';
    }

}
