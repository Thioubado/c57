<?php

namespace Grcote7\Tutos\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Flash;

class MyController extends Controller
{
    public $implement = [];

    public $listConfig = 'config_mycontroller.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Grcote7.Tutos', 'main-menu-item');
    }
    

public function index(){

    return \Backend::redirect('grcote7/tutos/mycontroller/helloworld');
}

    public function index2()
    {
        // Retourne cette chaîne directement dans le backend
        // return 'Hello';
        // Check storage/logs/system.log

        debug('index2');
        
        $config = $this->makeConfig('$/grcote7/tutos/models/tuto/columns.yaml');

        $config->model = new \GrCote7\tutos\Models\Tuto;

        $config->recordUrl = 'grcote7/tutos/myController/update/:id';
    
        $widget = $this->makeWidget('Backend\Widgets\Lists', $config);

        $widget->bindToController();
    
        $this->vars['widget'] = $widget;
    }

    public function helloworld()
    {
        // Retourne cette chaîne
        // return 'Hello World !';

        // S'il n'y a rien, va chercher le .htm du dossier mycontroller
    }

    // /URL = be/grcote7/tutos/mycontroller/update/7
    // /URL = be/grcote7/tutos/mycontroller/update/7/foobar
    public function update($id = null, $context = null)
    {
        $this->vars['myId'] = $this->getIdVide($id);
        debug($id);
        debug($this->vars['myId']);
        debug('update()');

        // Check storage/logs/system.log

        $this->vars['myId'] .= '<hr>'.(('7' === $id) ? 'OK, you respect the exemple of the code...' : 'Try with param 1...');
        //trace_log('update was called with ID = '.$id);

        $config = $this->makeConfig('$/grcote7/tutos/models/tuto/fields.yaml');

        if ($id<>1 && $id<>2){
            $id=2;
        } 
        $config->model = \GrCote7\Tutos\Models\Tuto::find($id);

        $widget = $this->makeWidget('Backend\Widgets\Form', $config);

        $this->vars['widget'] = $widget;

    }

    public function affMsg()
    {
        debug('affmsg()');
    }
    
    public function update_onUpdate($id = null) // Action Ajax
    {
        Flash::success('Jobs done!');
        // Check storage/logs/system.log
        trace_log('onUpdate was called with ID = '.$id);
    }

    public function getIdVide($id){
        return (($id)?$id:'Vide');
    }
    
        public function onUpdate3($id = null)
    {
        $data = post();
    
        // Check storage/logs/system.log
        trace_log($data);
    
        Flash::success('Jobs done! (Logs updated)');
    }
}
