<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Profile\Components;

use Cms\Classes\ComponentBase;

class UsersList extends ComponentBase
{
    protected $r;

    public function componentDetails()
    {
        return [
            'name'        => 'usersList Component',
            'description' => 'No description provided yet...',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $r       = User::getList();
        $this->r = $r;
        $this->aff();
    }

    protected function aff()
    {
        $this->page['data'] = print_r($this->r, true);
    }
}
