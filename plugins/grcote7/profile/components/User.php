<?php

/*
 * Ce fichier est la propriété de C57.fr
 *
 * (c) Membre@c57.fr - 2019
 *
 * Et C57... C'est à VOUS !
 *
 * Sérieusement, ce fichier source est sujet à la license MIT*.
 * Mais je compte sur vous pour toujours chercher à l'améliorer et à votre tour, en faire profiter
 * un max de monde grâce aux techniques offertes dans c57.fr.
 *
 * @Bi1tô, & Très Bonne Année 2019.
 *
 *  *: En gros...: Vous en faites ce que vous voulez !!!
 */

namespace Grcote7\Profile\Components;

use Carbon\Carbon;
use GrCOTE7\Profile\Models\Profile;
use Jenssegers\Date\Date;
use RainLab\User\Models\User as UserRL;

class User extends UserRL
{
    public function __contruct()
    {
        $this->sepa();
    }

    public static function getList()
    {
        self::sepa();

        // $r[] = UserRL::select('id', 'name')->get();
        $r[]   = 'id / Name';
        $users = self::all();

        foreach ($users as $user) {
            $r[] = $user->id.' '.$user->name;
        }
        $r[] = SEPA;

        $r[]      = 'Uid / Pseudo / Parrain / Sexe';
        $profiles = Profile::all();

        foreach ($profiles as $profile) {
            $r[] = $profile->user_id.' '.$profile->pseudo.' '.$profile->parr.' '.$profile->sexe;
        }

        $r[] = SEPA;

        $r[] = 'user->pseudo de user 2: '.($users[1]->profile->pseudo ?? 'Indéfini');

        $r[] = SEPA;

        // $date = Carbon::now()->locale('fr');
        // $r[]  = $date->locale();

        return $r;
    }

    private static function sepa()
    {
        if (!(\defined('SEPA'))) {
            \define('SEPA', str_repeat('- ', 15));
        }
    }
}
